<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class Phones extends ActiveRecord
{    
    public static function tableName()
    {
        return 'phones';
    }
    public static function get_phones($id){
        $rows =  Phones::find()
            ->where(['contact_id' => $id])
            ->asArray()
            ->all();
        return $rows;
    }    
}
