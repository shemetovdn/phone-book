<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class Contacts extends ActiveRecord
{

    public static function tableName()
    {
        return 'contacts';
    }
    public function rules()
    {
        return [[['name'],
                'required', 'message' => "Поле не может быть пустым"],
            ];
    }
    public function contact_lists()
    {
        $rows = new ActiveDataProvider(['query' => 
            Contacts::find()->
            select('contacts.*')->
            orderBy('id DESC')->
            asArray()
            , 'pagination' => ['pageSize' => 10, ], ]);
        return $rows;
    }       

}
