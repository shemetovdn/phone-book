<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'View';
$this->params['breadcrumbs'][] = $this->title;
?>
<label>Name:</label>
<p><?php echo $model->name;?></p>
<label>Surname:</label>
<p><?php echo $model->surname;?></p>
<label>Patronymic:</label>
<p><?php echo $model->patronymic;?></p>
<?php  $count = 1;
 foreach($phones as $key => $value){?>
<label>Phone Number <?php echo $count;?>:</label>
<p><?php echo $value['phone'];?></p>
<?php $count++; }?>