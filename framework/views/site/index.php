<?php

/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = 'Phone Book';
?>
<a href="/site/save" class="btn btn-primary">add</a>
<?php
     echo GridView::widget([
         'dataProvider' => $post_list,
         'filterModel' => $searchModel,
         //'showFooter'=>true,
         //'showHeader' => true,
         'options' => ['style' => 'max-height:30px;', 'max-width:10px;',],
         'columns' => [
               [
                 'attribute' => 'Id',
                 'value' => 'id'
               ],
               [
                 'attribute' => 'Name',
                 'label'=>'Name',
                 'format' => 'html',
                 'value' => function ($model, $key, $index, $widget) {
                    return '<p class="Name">'.$model['name'].'</p>';
                  }
               ],
               [
                 'attribute' => 'Surname',
                 'label'=>'Surname',
                 'format' => 'html',
                 'value' => function ($model, $key, $index, $widget) {
                    return '<p class="surname">'.$model['surname'].'</p>';
                  }
               ], 
               [
                 'attribute' => 'Patronymic',
                 'label'=>'Patronymic',
                 'format' => 'html',
                 'value' => function ($model, $key, $index, $widget) {
                    return '<p class="patronymic">'.$model['patronymic'].'</p>';
                  }
               ],              
               [
                 'attribute' => 'Действие',
                 'format' => 'html',
                 'label'=>'Действие',
                 'value' => function ($model, $key, $index, $widget) {
                    return '<a class="btn btn-danger " href="/site/delete?&id='.$model['id'].'">Delete</a>
                    <a class="btn btn-warning " href="/site/save?&id='.$model['id'].'">Edit</a>
                    <a class="btn btn-primary " href="/site/view?&id='.$model['id'].'">View</a>
                    ';
                  }
               ],

          ],
         'tableOptions' =>['class' => 'table table-striped table-bordered'],
     ]);
?>
