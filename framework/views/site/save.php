<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
    $this->title = 'Save';

$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = yii\bootstrap\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'form-horizontal']]);?> 
<div class=" controls_panel">

    <?= $form->field($model, 'name')->input('text',['value' => $model->name]) ?>
    <?= $form->field($model, 'surname')->input('text',['value' => $model->surname]) ?>
    <?= $form->field($model, 'patronymic')->input('text',['value' => $model->patronymic]) ?>

</div>
    <div ng-app="Pones">
        <h2>Phones</h2>
        <div class=" item"  ng-controller="PhonesController as phones">
            <div class="form-group" id="phones" ng-init="phones.init(<?php echo $model->id;?>)">
                <ul ng-repeat="entity in phones.phones_list | filter:query as results">
                    <li class="btn close" ng-click="phones.delete_entity(entity.id, entity.contact_id)">x</li>
                    <li>
                        <label class="control-label" >Phone:</label>
                        <input type="text" class="form-control" name="Contacts[phones][{{$index}}][phone]" value="{{entity.phone}}">
                    </li>
                    <input type="hidden" name="Contacts[phones][{{$index}}][id]" value="{{entity.id}}">


                </ul>
                <ul>
                    <li>
                        <label class="control-label" >Phone:</label>
                        <input type="text" class="form-control" name="Contacts[phones][{{count= results.length}}][phone]" value="">
                    </li>
                    <hr/>
                </ul>
            </div>
    	<div class="form-group">
            <p class="btn btn-primary"ng-click="phones.add_entity($event)" data-count="{{count}}">+</p>
        </div>
        </div>
    </div>
	<div class="form-group">
        <?=Html::submitButton('Сохранить',['class' => 'btn btn-primary']);?>
    </div>
<?php yii\bootstrap\ActiveForm::end(); ?>