<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Contacts;
use app\models\Phones;

class SiteController extends Controller
{
    /**
     * Displays Phone Book.
     *
     * @return string
     */
    public function actionIndex()
    {
        $contacts = new Contacts;
        $list = $contacts->contact_lists();

        return $this->render('index', ['post_list' => $list]);
    }

    /**
     * View action.
     *
     * @return string
     */
    public function actionView($id = false)
    {
        $model = Contacts::findOne($id);
        $phones = Phones::get_phones($id);
        
        return $this->render('view', ['model' => $model,'phones'=> $phones]);
    }

    /**
     * Save action.
     *
     * @return string
     */
    
    public function actionDelete($id = false)
    {
        if(Contacts::findOne($id)->delete()){
            return $this->redirect(['site/']);   
        }

    }  

    public function actionSave($id = false)
    {
        if(!$model = Contacts::findOne($id)){
            $model = new Contacts();
        }
        $pones = Phones::find()->asArray()->orderBy('id')->all();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $form = Yii::$app->request->post('Contacts');
            $model->name = $form['name'];
            $model->surname = $form['surname'];
            $model->patronymic = $form['patronymic'];
            $model->save();
            $model_id =$model->id;
            $phones = $form['phones'];
            
            if(!empty($phones)){
                foreach($phones as $key=>$value){
                        if(!$model_phone = Phones::findOne($value['id'])){
                            $model_phone = new Phones();
                        }
                        if(!empty($value['phone'])){
                       $model_phone->phone = $value['phone'];
                        $model_phone->contact_id = $model_id;
                        $model_phone->save();                            
                        }
                }
            }
             return $this->redirect(['site/']);
        } else {
            return $this->render('save', ['model' => $model]);
        }
    } 
         
    public function actionGet_phones(){
        $request = Yii::$app->request;
        if ($request->post()) {
            $id = $request->post('id');
            $phones = Phones::get_phones($id);
            echo json_encode($phones);
        }
    }
    
    public function actionDeletePhone()
    {
        $request = Yii::$app->request;
        if ($request->post()) {
            $id = $request->post('id');
            $contact_id = $request->post('contact_id');
            $service = Phones::findOne($id);
            if ($service->delete()) {
                $phones = Phones::get_phones($contact_id);
                echo json_encode($phones);
            }
        }
    }    

}
