var admin = angular.module('Pones', []);

admin.controller('PhonesController', function( $http) {
    var Entity = this;
    Entity.init = function(id){
        $http({
            method: 'POST',
            url: '/site/get_phones',
            data: "id="+id+'&_csrf='+ yii.getCsrfToken(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            Entity.phones_list = response.data;
        });
    }

    Entity.add_entity = function(e){
        var id = +$(e.target).attr('data-count') + 1;
        if(id <= 9){
        $(e.target).attr('data-count', id);
        var ul = `
<ul>
            <li>
            <label class="control-label" >Phone:</label>
        <input type="text" class="form-control" name="Contacts[phones][`+id+`][phone]" value="">
            </li>


            </ul>
        `;

        $('#phones').append(ul);            
        }

    }
    Entity.delete_entity = function(id, contact_id){
        $http({
            method: 'POST',
            url: '/site/delete-phone',
            data: "id="+id+'&contact_id='+contact_id+'&_csrf='+ yii.getCsrfToken(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
            Entity.phones_list = response.data;
        });
    }

});